//
//  UpdateViewController.m
//  Empleados
//
//  Created by centro docente de computos on 5/15/15.
//  Copyright (c) 2015 UdeM. All rights reserved.
//

#import "UpdateViewController.h"
#import "Empleado.h"

@interface UpdateViewController (){
    Empleado *actualizarEmpleado;
    Empleado *buscarEmpleado;
}

@end

@implementation UpdateViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    actualizarEmpleado = [[Empleado alloc]init];
    buscarEmpleado = [[Empleado alloc]init];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)buscarbutton:(id)sender {
    buscarEmpleado.empCedula = _cedulatxt.text;
    [buscarEmpleado searchEmployedInDataBasebyId];
    
    _cedulatxt.text=buscarEmpleado.empCedula;
    _nombretxt.text=buscarEmpleado.empName;
    _direcciontxt.text=buscarEmpleado.empAdress;
    _edadtxt.text=buscarEmpleado.empAge;
    
    _statusLabel.text=buscarEmpleado.status;
}



- (IBAction)actualizarbutton:(id)sender {
    
    
    actualizarEmpleado.empName = _nombretxt.text;
    actualizarEmpleado.empCedula=_cedulatxt.text;
    actualizarEmpleado.empAdress= _direcciontxt.text;
    actualizarEmpleado.empAge=_edadtxt.text;
    
    [actualizarEmpleado updateEmployedInDataBase];
    
    _statusLabel.text = actualizarEmpleado.status;
}



@end
