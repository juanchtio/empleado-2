//
//  MenuViewController.h
//  Empleados
//
//  Created by centro docente de computos on 5/20/15.
//  Copyright (c) 2015 UdeM. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuViewController : UIViewController

- (IBAction)salirButton:(id)sender;
@end
