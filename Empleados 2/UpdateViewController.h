//
//  UpdateViewController.h
//  Empleados
//
//  Created by centro docente de computos on 5/15/15.
//  Copyright (c) 2015 UdeM. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UpdateViewController : UIViewController

@property (strong, nonatomic) IBOutlet UITextField *cedulatxt;
@property (strong, nonatomic) IBOutlet UITextField *nombretxt;
@property (strong, nonatomic) IBOutlet UITextField *direcciontxt;
@property (strong, nonatomic) IBOutlet UITextField *edadtxt;
@property (strong, nonatomic) IBOutlet UILabel *statusLabel;



- (IBAction)buscarbutton:(id)sender;
- (IBAction)actualizarbutton:(id)sender;

@end
