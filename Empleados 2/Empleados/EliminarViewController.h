//
//  EliminarViewController.h
//  Empleados
//
//  Created by centro docente de computos on 5/19/15.
//  Copyright (c) 2015 UdeM. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Empleado.h"
@interface EliminarViewController : UIViewController

@property (strong, nonatomic) IBOutlet UITextField *txtcedula;
@property (strong, nonatomic) IBOutlet UILabel *status;

- (IBAction)EliminarButton:(id)sender;

@end
